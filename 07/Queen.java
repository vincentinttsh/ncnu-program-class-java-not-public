import java.util.Scanner;
public class Queen {
    static int times = 1; // 印到第幾個
    static boolean have_done = false;//是否有印
    public static void queen(int n){
        int[][] ans = new int[n][n]; //棋盤
        for (int i = 0 ; i < n ; i++)
            for(int j = 0 ; j < n ; j++)
                ans[i][j] = 0;
        queen(n, new boolean[n], new boolean[2*n-1], new boolean[2*n-1], 0, ans);
    }
                            //有幾個row, 該col是否有棋, slash, back slash, 試到第幾個row, 棋盤 
    private static void queen(int n, boolean[] col, boolean[] ur, boolean[] dr, int row, int[][] ans){
        if (n == row){
            have_done = true;
            print(ans);
        }
        for(int c = 0 ; c < n ; c++){
            if(!col[c] && !ur[row+c] && !dr[row-c+n-1] ){
                col[c] = ur[row+c] = dr[row-c+n-1] = true;
                ans[row][c] = 1;
                queen(n, col, ur, dr, row+1, ans) ;
                col[c] = ur[row+c] = dr[row-c+n-1] = false;
                ans[row][c] = 0;
            }
        }
    }
    private static void print(int[][] ans){//印出解
        System.out.println("第 "+ times++ + " 解");
        for(int[] x : ans){
            for(int y : x)
                System.out.print(y+" ");
            System.out.println();
        }
        System.out.println("----------------");
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        queen(input.nextInt());
        if (!have_done)//無解
            System.out.println("找不到解");
        input.close();
    }
}
import java.util.Scanner;
public class Sudoku {
    public static void sudoku(int now , int[][] data, boolean[][] row, boolean[][] col, boolean[][] square){
        if(now == 81){
            System.out.println("------------------------");
            print(data);
            return;
        }
        int i = now / 9 , j = now % 9;
        if(data[i][j] == 0){
            for(int k = 1 ; k<= 9 ; k++){
                if(!col[j][k] && !row[i][k] && !square[(j/3)+(3*(i/3))][k]){
                    data[i][j] = k;
                    col[j][k] = row[i][k] = square[(j/3)+(3*(i/3))][k] = true;
                    sudoku(now+1, data, row, col, square);
                    data[i][j]=0;
                    col[j][k] = row[i][k] = square[(j/3)+(3*(i/3))][k] = false;
                }
            }
                return;
        }
        sudoku(now+1, data, row, col, square);
        return;
    }
    public static void print(int[][] ans){
        for(int[] x : ans){
            for(int y : x){
                System.out.print(y + " ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        int[][] data = new int[9][9];
        boolean[][] square = new boolean[9][10];
        boolean[][] row = new boolean[9][10];
        boolean[][] col = new boolean[9][10];
        Scanner input = new Scanner(System.in);
        for (int i = 0 ; i < data.length ; i++)
            for(int j = 0 ; j < data[i].length ; j++){
                data[i][j] = input.nextInt();
                col[j][data[i][j]] = 
                row[i][data[i][j]] = 
                square[(j/3)+(3*(i/3))][data[i][j]] = true;
            }
        sudoku(0, data , row , col , square);
        input.close();
    }
}
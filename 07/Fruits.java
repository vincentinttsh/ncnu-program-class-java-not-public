import java.util.Scanner;
pubic class Fruits {
    static boolean have_buy = false;
                            // 剩錢,還剩幾個沒買,水果價格,買的,挑到哪
    public static void Buy(int money, int n, int[] fruit, int[] have, int pos) {
        if (n == 0) {// 買完了
            for (int i = 0; i < fruit.length - 1; i++) {
                System.out.print(fruit[i] + "元的 " + have[i] + " 個 、");
            }
            System.out.println(fruit[fruit.length - 1] + "元的 " + have[fruit.length - 1] + " 個");
            have_buy = true;
            return;
        }
        if(pos >= fruit.length)//不能超過
            return;
        int m = 0;// 該水果個數
        while (money >= fruit[pos] * m && n >= m) { //錢夠且尚須購買
            have[pos] = m;
            Buy(money - fruit[pos] * m, n - m, fruit, have, pos + 1);
            m++;
        }
        have[pos] = 0;//歸零
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int money = input.nextInt();
        int n = input.nextInt();
        int species = input.nextInt();
        int[] fruit = new int[species];
        for (int i = 0; i < fruit.length; i++)
            fruit[i] = input.nextInt();
        Buy(money, n, fruit, new int[species], 0);
        if (have_buy == false)// 沒有買成功
            System.out.println("無法買滿");
        input.close();
    }
}
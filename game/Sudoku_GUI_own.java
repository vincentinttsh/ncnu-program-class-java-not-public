import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Sudoku_GUI_own implements ActionListener {
    private JFrame program;
    private Sudoku SudokuBoard;

    private Sudoku_GUI_own(byte[][] board, byte[][] where_square, boolean[][] square, boolean[][] row,
            boolean[][] col) {
        this.program = new JFrame("數獨遊戲");
        JMenu m;
        JMenuBar mb;
        this.SudokuBoard = new Sudoku(this.program, board, where_square, square, row, col);
        CloseWindow close = new CloseWindow(this.program, true);
        this.program.setJMenuBar(mb = new JMenuBar());
        mb.add(m = new JMenu("遊戲")).add(new JMenuItem("解答")).addActionListener(this);
        mb.add(new JMenu("檢查")).add(new JMenuItem("檢查此答案")).addActionListener(this);
        m.add(new JMenuItem("結束")).addActionListener(close);
        mb.add(new JMenu("說明")).add(new JMenuItem("關於本遊戲")).addActionListener(this);
        this.program.addWindowListener(close);
        this.SudokuBoard.Fill();
        this.program.setVisible(true);
    }

    public static void main(String[] args) {
        byte[][] board = new byte[9][9], where_square = new byte[9][9];
        boolean[][] square = new boolean[9][10], row = new boolean[9][10], col = new boolean[9][10];
        try {
            FileReader fr = new FileReader("data.txt");
            BufferedReader br = new BufferedReader(fr);
            while (br.ready()) {
                int i = 0;
                while (i < 81) {
                    String[] temp = br.readLine().split(" ");
                    for (int k = 0; k < temp.length; k++) {
                        where_square[i / 9][i % 9] = (byte) (temp[k].charAt(0) - '0');
                        i++;
                    }
                }
                int y = 0, j;
                while (y < 81) {
                    String te = br.readLine();
                    String[] temp;
                    if (te.equals(""))
                        continue;
                    else
                        temp = te.split(" ");
                    for (int k = 0; k < temp.length; k++) {
                        i = y / 9;
                        j = y % 9;
                        board[i][j] = (byte) (temp[k].charAt(0) - '0');
                        square[where_square[i][j]][board[i][j]] = col[j][board[i][j]] = row[i][board[i][j]] = true;
                        y++;
                    }
                }
            }
            fr.close();
        } catch (Exception e) {
            new Error(new JFrame("數獨遊戲"), "警告", "讀取data.txt失敗");
        }
        new Sudoku_GUI_own(board, where_square, square, row, col);
    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (command.equals("關於本遊戲")) {
            new Error(program, "關於本遊戲", "v1.0");
        } else if (command.equals("解答")) {
            this.SudokuBoard.Answer();
        } else if (command.equals("檢查此答案")) {
            this.SudokuBoard.Check();
        }
    }
}

class JTextFieldLimit extends PlainDocument {
    private static final long serialVersionUID = 1L;
    private int limit;

    JTextFieldLimit(int limit) {
        super();
        this.limit = limit;
    }

    JTextFieldLimit(int limit, boolean upper) {
        super();
        this.limit = limit;
    }

    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
        if (str == null)
            return;

        if ((getLength() + str.length()) <= limit) {
            super.insertString(offset, str, attr);
        }
    }
}

class Sudoku {
    private boolean[][] square = new boolean[9][10], row = new boolean[9][10], col = new boolean[9][10];
    private Component[] all_Components = new Component[81];
    private byte[][] where_square = new byte[9][9], board = new byte[9][9];
    private Dimension mySize = new Dimension(600, 600);
    private JFrame parent;
    private Color[] colors = { Color.ORANGE, Color.BLUE, Color.YELLOW, Color.CYAN, Color.LIGHT_GRAY, Color.GREEN,
            Color.MAGENTA, Color.PINK, Color.WHITE };

    public Sudoku(JFrame p, byte[][] board, byte[][] where_square, boolean[][] square, boolean[][] row,
            boolean[][] col) {
        this.parent = p;
        this.where_square = where_square;
        this.board = board;
        this.row = row;
        this.col = col;
        this.square = square;
    }

    private void sudoku_ans(int now, byte[][] data, boolean[][] row, boolean[][] col, boolean[][] square,
            byte[][] where_square, boolean[][] have_fill) {
        if (now == 81) { // 填完了
            for (int k = 0; k < 81; k++) {
                int i = k / 9, j = k % 9;
                if (have_fill[i][j]) {
                    JTextField temp = (JTextField) this.all_Components[k];
                    temp.setText("" + data[i][j]);
                }
            }
            return;
        }
        int i = now / 9, j = now % 9;
        if (data[i][j] == 0) { // 未被填
            for (byte k = 1; k <= 9; k++) // 各數皆試
                if (!col[j][k] && !row[i][k] && !square[where_square[i][j]][k]) {
                    data[i][j] = k;
                    have_fill[i][j] = col[j][k] = row[i][k] = square[where_square[i][j]][k] = true;
                    sudoku_ans(now + 1, data, row, col, square, where_square, have_fill);
                    data[i][j] = 0;
                    have_fill[i][j] = col[j][k] = row[i][k] = square[where_square[i][j]][k] = false;
                }
            return;
        } // 已被填的,就到下一個
        sudoku_ans(now + 1, data, row, col, square, where_square, have_fill);
    }

    private void Addprogram(Component component, int gridx, int gridy, int gridwidth, int gridheight, int fill,
            int anchor, double weightx, double weighty) {
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = anchor;
        c.fill = fill;
        c.gridheight = gridheight;
        c.gridwidth = gridwidth;
        c.gridx = gridx;
        c.gridy = gridy;
        c.weightx = weightx;
        c.weighty = weighty;
        c.insets = new Insets(1, 1, 1, 1);
        this.parent.add(component, c);
    }

    public void Fill() {
        this.parent.setLayout(new GridBagLayout());
        this.parent.setSize(this.mySize);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (this.board[i][j] != 0) {
                    JLabel temp = new JLabel("" + this.board[i][j]);
                    temp.setOpaque(true);
                    temp.setHorizontalAlignment(JLabel.CENTER);
                    temp.setBackground(colors[this.where_square[i][j]]);
                    this.all_Components[i * 9 + j] = temp;
                } else {
                    JTextField temp = new JTextField(1);
                    temp.setFont(new Font("Serif", Font.ITALIC, 14));
                    temp.setDocument(new JTextFieldLimit(1));
                    temp.setHorizontalAlignment(JTextField.CENTER);
                    temp.setBackground(colors[this.where_square[i][j]]);
                    this.all_Components[i * 9 + j] = temp;
                }
            }
        }
        for (int i = 0; i < 81; i++)
            Addprogram(all_Components[i], 2 * (i % 9), 2 * (i / 9), 2, 2, GridBagConstraints.BOTH,
                    GridBagConstraints.WEST, 1, 1);
    }

    public void Answer() {
        sudoku_ans(0, this.board.clone(), this.row.clone(), this.col.clone(), this.square.clone(), this.where_square,
                new boolean[9][9]);
    }

    public void Check() {
        byte[][] c_data = new byte[9][9];
        boolean[][] c_square = new boolean[9][10];
        boolean[][] c_row = new boolean[9][10];
        boolean[][] c_col = new boolean[9][10];
        for (int k = 0; k < 81; k++) {
            int i = k / 9, j = k % 9;
            if (this.board[i][j] == 0) {
                JTextField temp = (JTextField) this.all_Components[k];
                if (temp.getText().equals("")) {
                    new Error(this.parent, "警告", "未輸入完成");
                    return;
                } else {
                    byte num = (byte) (temp.getText().charAt(0) - '0');
                    if (num < 0 || num > 9) {
                        new Error(this.parent, "警告", "輸入非法字元");
                        return;
                    } else if (num == 0) {
                        new Error(this.parent, "警告", "輸入1~9");
                        return;
                    } else {
                        c_data[i][j] = num;
                    }
                }
            } else {
                c_data[i][j] = this.board[i][j];
            }
        }
        for (int k = 0; k < 81; k++) {
            int i = k / 9, j = k % 9;
            if (!c_col[j][c_data[i][j]] && !c_row[i][c_data[i][j]] && !c_square[where_square[i][j]][c_data[i][j]]) {
                c_square[where_square[i][j]][c_data[i][j]] = c_col[j][c_data[i][j]] = c_row[i][c_data[i][j]] = true;
            } else {
                new Error(parent, "警告", "輸入錯誤");
                return;
            }
        }
        new Error(parent, "恭喜", "輸入正確");
    }
}

class Error extends JDialog {
    private static final long serialVersionUID = 1L;
    private Font font = new Font("Serif", Font.PLAIN, 16);

    public Error(JFrame parent, String title, String message) {
        super(parent, title, true);
        setBounds(100, 200, 250, 200);
        JLabel l = new JLabel(message);
        l.setFont(this.font);
        add(BorderLayout.NORTH, l);
        JButton b = new JButton("確定");
        add(BorderLayout.SOUTH, b);
        CloseWindow cw = new CloseWindow(this);
        this.addWindowListener(cw);
        b.addActionListener(cw);
        setVisible(true);
    }
}

class CloseWindow extends WindowAdapter implements ActionListener {
    private Window target;
    private boolean exit;

    public CloseWindow(Window target, boolean exit) {
        this.target = target;
        this.exit = exit;
    }

    public CloseWindow(Window target) {
        this.target = target;
    }

    public void windowClosing(WindowEvent e) {
        target.dispose();
        if (exit)
            System.exit(0);
    }

    public void actionPerformed(ActionEvent e) {
        target.dispose();
        if (exit)
            System.exit(0);
    }
}
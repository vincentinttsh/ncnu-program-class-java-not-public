import java.util.Scanner;
class Poly{
    int power,coe;
    public Poly(int coe, int power){
        this.coe = coe;
        this.power = power;
    }
}
public class PolyMul_s {
    Poly[][] fun = new Poly[3][];
    int[] size = new int[3];
    public PolyMul_s(String[][] num){
        for(int i = 0; i < 2; i++){
            fun[i] = new Poly[num[i].length/2];
            for(int j = 0; j < num[i].length; j+=2){
                if(index(Integer.valueOf(num[i][j+1]), i) != -1){
                    fun[i][index(Integer.valueOf(num[i][j+1]), i)].coe += Integer.valueOf(num[i][j]);
                }else{
                    fun[i][size[i]++] = new Poly(Integer.valueOf(num[i][j]), Integer.valueOf(num[i][j+1]));
                }
            }
        }
    }
    public void print(){
        for(int j = size[2]; j>=0; j--)
            System.out.print(fun[2][j].coe + "x^" + fun[2][j].power+" ");
        System.out.println();
    }
    private int index(int power, int fun_num){
        for(int i = 0; i < size[fun_num]; i++)
            if(fun[fun_num][i].power == power)
                return i;
        return -1;
    }
    public void mul(){
        fun[2] = new Poly[size[0] * size[1]];
        for(int i = 0 ; i < size[0]; i++){
            for(int j = 0; j < size[1]; j++){
                Poly x = fun[0][i], y = fun[1][j];
                int coe = x.coe * y.coe, power = x.power + y.power;
                if(index(power, 2) != -1){
                    fun[2][index(power, 2)].coe += coe;
                }else{
                    fun[2][size[2]++] = new Poly(coe, power);
                }
            }
        }
        qsort(fun[2], 0, size[2]-1);
        print();
    }
    private void qsort(Poly[] data,int left,int right) {
        if (left >= right)
            return;
        Poly v = data[left];
        int l , r;
        for(l = left , r = right+1 ; l < r ; ){
            while(++l < right && data[l].power <= v.power);
            while(--r >= l && data[r].power >= v.power);
            if(l <= r){
                Poly temp = data[r];
                data[r] = data[l];
                data[l] = temp;
            }
        }
        data[left] = data[r];
        data[r] = v;
        qsort(data, left, r);
        qsort(data, r+1, right);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[][] num = new String[2][];
        num[0] = input.nextLine().split(" ");
        num[1] = input.nextLine().split(" ");
        input.close();
        new PolyMul_s(num).mul();
    }
}
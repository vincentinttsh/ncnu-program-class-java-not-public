import java.util.Scanner;

public class Light {
    private boolean light[][]; // 燈狀態
    private int size; // 長度
    public int ans;// 解個數
    private boolean have_press[][];// 按鍵狀態

    public Light(int n) {// 建構子
        light = new boolean[n][n];
        have_press = new boolean[n][n];
        size = n;
        ans = 0;
    }

    public void run() {// 遞迴找解
        if (size == 1) {
            System.out.println("#");
            ans += 1;
            System.out.println("---------");
            return;
        }
        run(0, light, have_press);
    }

    private void run(int now, boolean[][] light_a, boolean[][] have_press_a) { // 目前在哪, 燈狀態, 按鍵狀態
        // 檢查是否完成
        boolean done = true;
        for (boolean[] q : light_a)
            for (boolean w : q)
                if (!w) {
                    done = false;
                    break;
                }
        if (done) {
            ans += 1;
            print_press(have_press_a);
            System.out.println("---------");
            return;
        }
        // 到最後一格
        if (now == size * size)
            return;
        // 不按按鍵
        if (now / size == size - 1) {// 最後一排
            if (now % size != 0) {// 非第一個
                if (light_a[now / size - 1][now % size] && light_a[now / size][now % size - 1]) {
                    run(now + 1, light_a, have_press_a);
                }
            } else {// 第一個
                if (light_a[now / size - 1][now % size]) {
                    run(now + 1, light_a, have_press_a);
                }
            }
        } else if (now > size) {// 非第一排
            if (light_a[now / size - 1][now % size]) {
                run(now + 1, light_a, have_press_a);
            }
        } else {// 第一排
            run(now + 1, light_a, have_press_a);
        }
        // 按按鍵
        if (now / size == size - 1) {// 最後一排
            if (now % size != 0) {// 非第一個
                if (!light_a[now / size - 1][now % size] && !light_a[now / size][now % size - 1]) {
                    press(have_press_a, now / size, now % size);
                    changeStatus(light_a, now / size, now % size);
                    run(now + 1, light_a, have_press_a);
                    press(have_press_a, now / size, now % size);
                    changeStatus(light_a, now / size, now % size);
                }
            } else {// 第一個
                if (!light_a[now / size - 1][now % size]) {
                    press(have_press_a, now / size, now % size);
                    changeStatus(light_a, now / size, now % size);
                    run(now + 1, light_a, have_press_a);
                    press(have_press_a, now / size, now % size);
                    changeStatus(light_a, now / size, now % size);
                }
            }
        } else if (now > size) {// 非第一排
            if (!light_a[now / size - 1][now % size]) {
                press(have_press_a, now / size, now % size);
                changeStatus(light_a, now / size, now % size);
                run(now + 1, light_a, have_press_a);
                press(have_press_a, now / size, now % size);
                changeStatus(light_a, now / size, now % size);
            }
        } else {// 第一排
            press(have_press_a, now / size, now % size);
            changeStatus(light_a, now / size, now % size);
            run(now + 1, light_a, have_press_a);
            press(have_press_a, now / size, now % size);
            changeStatus(light_a, now / size, now % size);
        }
    }

    // 按下開關
    private void press(boolean[][] q, int x, int y) {
        q[x][y] = !q[x][y];
    }

    // 改變狀態
    private void changeStatus(boolean[][] q, int x, int y) {
        if (x > 0)
            q[x - 1][y] = !q[x - 1][y];
        if (x != q.length - 1)
            q[x + 1][y] = !q[x + 1][y];
        if (y > 0)
            q[x][y - 1] = !q[x][y - 1];
        if (y != q.length - 1)
            q[x][y + 1] = !q[x][y + 1];
        q[x][y] = !q[x][y];
    }

    private void print_press(boolean[][] q) {
        for (boolean[] x : q) {
            for (boolean y : x) {
                if (y)
                    System.out.print("# ");
                else
                    System.out.print(". ");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Light ans = new Light(input.nextInt());
        input.close();
        ans.run();
        System.out.println("總共有" + ans.ans + "種解法");
    }
}
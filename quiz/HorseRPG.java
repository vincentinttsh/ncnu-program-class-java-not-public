import java.util.Scanner;
public class HorseRPG {
    private int m, n, ans, how_go_x[] = { -2, -1, 1, 2, 2, 1, -1, -2 }, how_go_y[] = { 1, 2, 2, 1, -1, -2, -2, -1 }, data[][];
    public HorseRPG(int n, int m, int x, int y) {
        data = new int[m][n];
        data[x][y] = 1;
        ans = 0;
        this.m = m;
        this.n = n;
        run(x, y, 2);
        System.out.println(ans);
    }
    public void run(int x, int y, int now) {
        if (now == m * n + 1) {
            ans++;
            return;
        }
        for (int i = 0; i < 8; i++)
            if (!(y + how_go_y[i] >= n || y + how_go_y[i] < 0 || x + how_go_x[i] >= m || x + how_go_x[i] < 0) && data[x + how_go_x[i]][y + how_go_y[i]] == 0) {
                data[x + how_go_x[i]][y + how_go_y[i]] = now;
                run(x + how_go_x[i], y + how_go_y[i], now + 1);
                data[x + how_go_x[i]][y + how_go_y[i]] = 0;
            }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        new HorseRPG(input.nextInt(), input.nextInt(), input.nextInt(), input.nextInt());
        input.close();
    }
}
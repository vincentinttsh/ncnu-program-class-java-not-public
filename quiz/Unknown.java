import java.math.BigInteger;
import java.util.Scanner;
public class Unknown {
    char[][] data = new char[3][];
    public Unknown(char[] x, char[] y, char[] z){
        this.data[0] = x;
        this.data[1] = y;
        this.data[2] = z;
    }
    
    public void start_guess(int now){
        if (now == data[0].length + data[1].length + data[2].length){
            String x = String.valueOf(data[0]), y = String.valueOf(data[1]), z = String.valueOf(data[2]);
            if(new BigInteger(x).multiply(new BigInteger(y)).equals(new BigInteger(z)))
                System.out.println(x + " * " + y + " = " + z);
            return;
        }
        char[] n_data;
        int pos;
        if(now < data[0].length){
            n_data = data[0];
            pos = now;
        } else if(now - data[0].length < data[1].length){
            n_data = data[1];
            pos = now - data[0].length;
        }else{
            n_data = data[2];
            pos = now - data[0].length - data[1].length;
        }
        if(n_data[pos] == '?'){
            for(int i = 0; i <= 9; i++){
                n_data[pos] = (char)('0' + i);
                start_guess(now + 1);
                n_data[pos] = '?';
            }
        }else
            start_guess(now + 1);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        new Unknown(input.next().toCharArray(), input.next().toCharArray(), input.next().toCharArray()).start_guess(0);
        input.close();
    }
}
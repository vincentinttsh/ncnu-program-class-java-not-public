import java.util.Vector;
import java.util.Random;
import java.util.Scanner;
class Card {
    private Vector<Integer> have;
    static String[] color = { "s", "h", "c", "d" };
    static String[] num = { "A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
    public Card() {
        have = new Vector<Integer>();
    }
    public Integer get_card(int index) {
        return have.get(index);
    }
    public void add(Integer x) {
        for (int i = 0; i < have.size(); i++) 
            if (x % 13 == have.get(i) % 13) {
                have.remove(i);
                return;
            }
        have.add(x);
    }
    public void remove(int index) {
        have.remove(index);
    }
    public int size() {
        return have.size();
    }
    public void print() {
        for (int i = 0; i < have.size(); i++)
            System.out.print(color[have.get(i) / 13] + num[have.get(i) % 13] + " ");
        System.out.println();
    }
}

public class PlayCard {
    public Card[] player;
    public PlayCard(int q) {
        Random ran = new Random();
        player = new Card[q];
        for (int i = 0; i < q; i++)
            this.player[i] = new Card();
        Integer[] temp = new Integer[52];
        for (int i = 0; i < 52; i++)
            temp[i] = (Integer) i;
        for (int i = 0; i < 1000; i++) {
            int a = ran.nextInt(52);
            int b = ran.nextInt(52);
            Integer t = temp[a];
            temp[a] = temp[b];
            temp[b] = t;
        }
        for (int i = 0; i < 51; i++) 
            this.player[i % q].add(temp[i]);
    }
    public void get_card(int x, int y, int q) {
        Random ran = new Random();
        int index = ran.nextInt(player[y].size());
        player[x].add(player[y].get_card(index));
        player[y].remove(index);
        System.out.println("player " + (x + 1) + " choose player " + (y + 1) + "'s " + (index + 1) + " th card");
    }
    public boolean game_over(int q) {
        int x = 0;
        for (int i = 0; i < q; i++)
            if (this.player[i].size() == 0)
                x++;
        if (x == q - 1)
            return true;
        return false;
    }
    public int loser(int q){
        for (int i = 0; i < q; i++)
            if (this.player[i].size() == 1)
                return i+1;
        return (q+2);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int many = input.nextInt();
        boolean[] have = new boolean[many];
        input.close();
        PlayCard game = new PlayCard(many);
        for (int i = 0; i < many; i++) {
            System.out.print("player " + (i + 1) + " Have: ");
            if (game.player[i].size() != 0)
                have[i]= true;
            game.player[i].print();
        }
        System.out.println("=== GAME START ===");
        int x = 0, now;
        while(!have[x])
            x++;
        now = x;
        while (!game.game_over(many)) {
            System.out.print(" Have: ");
            game.player[now].print();
            int next = (now+1) % many;
            while (!have[next])
                next = (next+1) % many;
            game.get_card(now, next, many);
            System.out.print(" Have: ");
            game.player[now].print();
            System.out.println("===");
            if(game.player[now].size() == 0)
                have[now] = false;
            if(game.player[next].size() == 0)
                have[next] = false;
            now = (now+1) % many;
            while (!have[now])
                now = (now+1) % many;
        }
        System.out.println("Loser: player " + game.loser(many));
    }
}
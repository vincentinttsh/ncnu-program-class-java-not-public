import java.util.Comparator;
import java.util.Scanner;
import java.util.Vector;
class Poly implements Comparable<Poly> {
    public int coe, power;
    public Poly(int x, int y) {
        coe = x;
        power = y;
    }
    public int compareTo(Poly o) {
        return o.power - this.power;
    }
}

public class PolyMul {
    private Vector<Poly> fun1 = new Vector<Poly>(), fun2 = new Vector<Poly>(), fun3 = new Vector<Poly>();
    public PolyMul(String[] temp1, String[] temp2) {
        for (int i = 0; i < temp1.length; i += 2) {
            if(index(Integer.valueOf(temp1[i + 1]), fun1) != -1){
                Poly x = fun1.get(index(Integer.valueOf(temp1[i + 1]), fun1));
                fun1.remove(x);
                x.coe += Integer.valueOf(temp1[i]);
                fun1.add(x);
            }else{
                Poly x = new Poly(Integer.valueOf(temp1[i]), Integer.valueOf(temp1[i + 1]));
                fun1.add(x);
            }
        }
        for (int i = 0; i < temp2.length; i += 2) {
            if(index(Integer.valueOf(temp2[i + 1]), fun2) != -1){
                Poly x = fun2.get(index(Integer.valueOf(temp2[i + 1]), fun2));
                fun2.remove(x);
                x.coe += Integer.valueOf(temp2[i]);
                fun2.add(x);
            }else{
                Poly x = new Poly(Integer.valueOf(temp2[i]), Integer.valueOf(temp2[i + 1]));
                fun2.add(x);
            }
        }
    }

    public void print() {
        for (int i = 0; i < fun3.size(); i++) {
            Poly x = fun3.get(i);
            if (x.coe == 0)
                continue;
            else if (i == 0) {
                if (x.power != 0)
                    System.out.print(x.coe + "x^" + x.power);
                else
                    System.out.print(x.coe);
            } else if (x.coe >= 0) {
                if (x.power != 0)
                    System.out.print("+" + x.coe + "x^" + x.power);
                else
                    System.out.print("+" + x.coe);
            } else {
                if (x.power != 0)
                    System.out.print(x.coe + "x^" + x.power);
                else
                    System.out.print(x.coe);
            }
        }
        System.out.println();
    }

    public void mul() {
        for (Poly x : fun1) {
            for (Poly y : fun2) {
                int power = x.power + y.power, coe = x.coe * y.coe;
                if(index(power, fun3) != -1){
                    Poly z = fun3.get(index(power, fun3));
                    fun3.remove(z);
                    z.coe += coe;
                    fun3.add(z);
                }else{
                    Poly z = new Poly(coe, power);
                    fun3.add(z);
                }
            }
        }
        fun3.sort(new Comparator<Poly>() {
            public int compare(Poly o1, Poly o2) {
                return o1.compareTo(o2);
            }
        });
        print();
    }

    private int index(int power, Vector<Poly> data){
        for(int i = 0; i < data.size(); i++){
            if(data.get(i).power == power)
                return i;
        }
        return -1;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        new PolyMul(input.nextLine().split(" "), input.nextLine().split(" ")).mul();
    }
}
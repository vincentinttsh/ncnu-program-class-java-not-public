public class Queue {
    private int size;
    private Node head, tail;
    public synchronized void append(int x){
        Node tmp = new Node();
        tmp.data = x;
        if (tail == null) // the first
            head = tail = tmp;
        else
            tail.next = tmp;
        tail = tmp;
        size++;
    }
    public synchronized int pop(){
        Node tmp = head;
        head = head.next;
        if (head == null)
            tail = null;
        size--;
        return tmp.data;
    }
    public synchronized boolean isempty(){
        return size == 0;
    }
    public static void main(String[] args) {
        Queue data = new Queue();
        for (int i =0 ; i < 10 ;i++)
            data.append(i);
        while(!data.isempty())
            System.out.println(data.pop());
    }
}
class Node{
    int data;
    Node next;
}
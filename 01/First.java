import java.util.Scanner;

public class First {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        Student[] student = new Student[n];
        for (int i = 0; i < n; i++) {
            student[i] = new Student(input.nextInt(), input.nextInt());
        }
        for (int i = 1; i < student.length; i++) {
            Student key = student[i];
            int j = i - 1;
            while (j >= 0 && student[j].grade < key.grade) {
                student[j + 1] = student[j];
                j--;
            }
            student[j + 1] = key;
        }
        System.out.println();
        for (Student temp : student) {
            temp.Print_all();
        }
        input.close();
    }
}

class Student {
    int no, grade;

    public Student(int no, int grade) {
        this.no = no;
        this.grade = grade;
    }

    public void Print_all() {
        System.out.println(this.no + " " + this.grade);
    }
}
import java.util.Scanner;
class Student {
    String name , no; //姓名 學號
    int math_grade , en_grade; //英文成績 數學成績
    public Student(String name , String no , int en_grade ,int math_grade){ //constructor
        this.name = name;
        this.no = no;
        this.en_grade = en_grade;
        this.math_grade = math_grade;
    }
    public double avg() { // return average
        return (this.en_grade + this.math_grade) / 2;
    }
}

public class Transcript {
    private Student[] student_list; 
    public void find(String id) { //find person and print
        for (Student x :this.student_list)
            if(x.no.equals(id))
                System.out.println(x.no + "\t" + x.name + "\t" + x.en_grade + "\t" + x.math_grade);
    }
    public void sort() { //insertion sort
        int i , j;
        for( i = 1; i < this.student_list.length; i++) {
            Student tmp = this.student_list[i];
            for( j = i; j > 0 && tmp.avg() > this.student_list[j - 1].avg(); j--) {
                this.student_list[j] = this.student_list[j - 1];
            }
            this.student_list[j] = tmp;
        }
    }
    public int size(){ //return the size of stduent list
        return student_list.length;
    }
    public void input(int now , String name , String no , int math_grade ,int en_grade){ // input data
        student_list[now] = new Student(name, no, math_grade, en_grade);
    }
    public void print() { 
        System.out.println("-------- 資管系 Transcript --------");
        System.out.println("姓名\t英文\t數學\t平均");
        for(Student x : student_list) 
            System.out.println(x.name + "\t" + x.en_grade + "\t" + x.math_grade + "\t" + x.avg());
        System.out.println("-------- 資管系 Transcript --------");
    }
    public Transcript(int n){ //constructor
        student_list = new Student[n];
    }
    public static void main(String[] argv) {
        Transcript grade_list;
        Scanner input = new Scanner(System.in);
        grade_list = new Transcript(input.nextInt());
        for (int i = 0 ; i < grade_list.size(); i++ ) //input data
            grade_list.input(i ,input.next() , input.next() , input.nextInt() , input.nextInt());
        grade_list.sort();
        grade_list.print();
        grade_list.find(input.next());
        input.close();
    }
}

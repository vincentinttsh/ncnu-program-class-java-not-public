import java.util.Scanner;
public class Matrix{
    static void Flip(int[][] x){ //Row i to x.length-i-1	
        for(int i = 0 ; i < x.length / 2 ; i++){
            int[] temp = x[i];
            x[i] = x[x.length-1-i] ; 
            x[x.length-1-i] = temp;
        }
    }
    static int[][] Rotate(int[][] x){
        int[][] temp = new int[x[0].length][x.length];  // Need new sqaure with new row and column
        for(int i = 0 ; i < x.length ; i++)
            for (int j = 0 ; j < x[i].length ; j++)
                temp[j][x.length-i-1] = x[i][j]; //input num to right place
        return temp;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int R = input.nextInt() ;
        int C = input.nextInt() ;
        int M = input.nextInt() ;
        int [][] square = new int[R][C];
        for (int i = 0 ; i < R ; i++)   //input num to square
            for(int j = 0 ; j < C ; j++)
                square[i][j]=input.nextInt();
        for (int k = 0 ;k < M ; k++){
            if(input.nextInt() == 1){
                Flip(square);
            }else{
                square = Rotate(square);
            }
        }
        System.out.println(square.length+" "+square[0].length); //print row and column
        for (int[] now_row : square ){ //print_all
            for(int now_num : now_row)
                System.out.print(now_num + " ");
            System.out.println();
        }
        input.close();
    } 
}

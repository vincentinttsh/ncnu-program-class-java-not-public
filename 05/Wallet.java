/*
* 學號 : 107213004
* 姓名 : 郭子緯
*/
class Wallet{
    private String owner;
    private int monney;
    public Wallet(String a , int b){
        owner = a;
        monney = b;
    }
    public void earn(int a){
        monney +=a;
    }
    public void spend(int a){
        monney-=a;
    }
    public void get_info(){
        System.out.println("Owner： "+owner+" money: "+monney);
    }
    public static void main(String[] args) {
        Wallet my_Wallet = new Wallet("vincentinttsh", 1000);
        my_Wallet.get_info();
    }
}
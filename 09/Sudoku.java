import java.util.Scanner;
public class Sudoku {
                            //位置, 數獨的格子, 該row哪幾個數字已有, 該col哪幾個數字已有, 拼圖, 每格屬於哪個拼圖
    public static void sudoku(int now , int[][] data, boolean[][] row, boolean[][] col, boolean[][] square, int[][] where_square){
        if(now == 81){ //填完了
            System.out.println("------------------------");
            print(data);
            return;
        }
        int i = now / 9 , j = now % 9;
        if(data[i][j] == 0){ //未被填
            for(int k = 1 ; k<= 9 ; k++) //各數皆試
                if(!col[j][k] && !row[i][k] && !square[where_square[i][j]][k]){
                    data[i][j] = k;
                    col[j][k] = row[i][k] = square[where_square[i][j]][k] = true;
                    sudoku(now+1, data, row, col, square, where_square);
                    data[i][j] = 0;
                    col[j][k] = row[i][k] = square[where_square[i][j]][k] = false;
                }
            return;
        }//以被填的,就到下一個
        sudoku(now+1, data, row, col, square, where_square);
    }
    public static void print(int[][] ans){
        for(int[] x : ans){
            for(int y : x)
                System.out.print(y + " ");
            System.out.println();
        }
    }
    public static void main(String[] args) {
        int[][] data = new int[9][9], where_square = new int[9][9];
        boolean[][] square = new boolean[9][10], row = new boolean[9][10], col = new boolean[9][10];
        Scanner input = new Scanner(System.in);
        for(int i = 0 ; i < 81 ; i++)
            where_square[i/9][i%9] = input.nextInt();
        for (int i = 0 ; i < data.length ; i++)
            for(int j = 0 ; j < data[i].length ; j++){
                data[i][j] = input.nextInt();
                square[where_square[i][j]][data[i][j]] = col[j][data[i][j]] = row[i][data[i][j]] = true;
            }
        sudoku(0, data, row, col, square, where_square);
        input.close();
    }
}
//3n+1
import java.util.Scanner;
public class First {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (input.hasNextInt()) {
            int a = input.nextInt();
            int b = input.nextInt();
            int maxn = 0;
            for (int i = Math.min(a, b); i <= Math.max(a, b); i++) {
                maxn = Math.max(maxn, n3(i));
            }
            System.out.println(a+" "+b+" "+maxn);
        }
        input.close();
    }

    public static int n3(int x) {
        int times=0;
        while (x != 1) {
            times += 1;
            if (x % 2 == 1) 
                x = 3 * x + 1;
            else
                x = x / 2;
        }
        return times+1;
    }
}


public class Quick_sort {
    public static void qsort(int[] data,int left,int right) {
        if (left >= right)
            return;
        int v = data[left] , l , r;
        for(l = left , r = right+1 ; l < r ; ){
            while(++l < right && data[l] <= v);
            while(--r >= l && data[r] >= v);
            if(l <= r){
                int temp = data[r];
                data[r] = data[l];
                data[l] = temp;
            }
        }
        data[left] = data[r];
        data[r] = v;
        qsort(data, left, r);
        qsort(data, r+1, right);
    }
    public static void main(String[] args) {
        int[] num = {1,2,7,6,3,4};
        qsort(num,0,num.length-1);
        for (int x : num)
            System.out.println(x);
    }
}
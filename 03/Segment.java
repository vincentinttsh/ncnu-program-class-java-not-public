import java.util.Scanner;
public class Segment{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        Line[] line = new Line[N];
        for (int i = 0 ; i < N ; i++) //得到數列
            line[i] = new Line(input.nextInt(),input.nextInt());
        System.out.println(findlong(sort(line)));
        input.close();
    }
    public static int findlong(Line[] line){
        int nowstart=line[0].start , nowend=line[0].end; // 設nowstart , nowend 為第一個線段的起點 , 終點
        int longline=0; //總長初始化
        for (int i = 1 ;i <line.length;i++){
            if (nowend>=line[i].start){ //該線段起點在範圍內
                nowend=Math.max(line[i].end,nowend);
            }else{ //結束該group，換到下一個group
                longline += nowend-nowstart;
                nowstart=line[i].start;
                nowend=line[i].end;
            }
        }
        return longline+nowend-nowstart;
    }
    //insertion sort
    public static Line[] sort(Line line[]){
        for (int i = 1; i < line.length; i++) {
            Line key = line[i];
            int j = i - 1;
            //該線段起點比key大的放在key後
            while (j >= 0 && line[j].start > key.start) {
                line[j + 1] = line[j];
                j--;
            }
            //key放回（不一定是原位）
            line[j + 1] = key;
        }
        return line;
    }
}
class Line{
    int start , end;
    public Line(int start, int end) {
        this.start = start;
        this.end = end;
    }
}

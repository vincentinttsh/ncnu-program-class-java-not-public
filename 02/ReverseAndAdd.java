import java.util.Scanner;
public class fourth {
    public static int reverse(int n) {
        int x =0 ;
        while(n!=0){
            x=(x*10)+(n%10);
            n/=10;
        }
        return x;
    }
    public static void isequal(int nums) {
        int times=0;
        while(nums != reverse(nums)){
            nums=nums+reverse(nums);
            times+=1;
        }
        System.out.println(times+" "+nums);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for(int i = 0 ; i< n ;i++){
            isequal(input.nextInt());
        }
        input.close();
    }
}
import java.util.Scanner;
public class Leap{
    public static void main(String[] argv) {
        Scanner input = new Scanner(System.in);
        int year = input.nextInt();
        System.out.println(isleap(year));
        input.close();
    }
    static boolean isleap(int n){
        if (n % 400 == 0 && n % 3200 != 0)
            return true;
        else if (n% 4 == 0 && n % 100 != 0)
            return true;
        else
            return false;
    }
}

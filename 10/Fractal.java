/**
 * Fractal
 */
public class Fractal extends Thread{
    public static Pen p;
    int len, x1 ,y1;
    public Fractal(int x1, int y1, int len) {
        this.len = len;
        this.x1 = x1;
        this.y1 = y1;
    }
    public void run (){
        tri(this.x1, this.y1, this.len);
    }
    public static void tri(int x1, int y1, int len) {
        if(len <= 1) {
            synchronized(p){
                p.flyTo(x1, y1);
                p.runTo(x1, y1);
            }
            try{
                Thread.currentThread().sleep(10);
            }catch(Exception err){}
            return;
        }
        // 算出三角形的底跟高
        // 算出三角形中更小的三角形的底跟高

        tri(x1, y1, len / 2);
        tri(x1 + len / 2, y1, len / 2);
        tri(x1 + len / 4, (int)(y1 - Math.sqrt(3)/4.0*len), len / 2);
    }
    public static void main(String[] args) {
        p = new Pen();
        Fractal f1 = new Fractal(10, 300, 128);
        Fractal f2 = new Fractal(10+128/2, 300, 128);
        Fractal f3 = new Fractal(10+128/4, (int)(300 - Math.sqrt(3)/4.0*128), 128);
        f1.start();
        f2.start();
        f3.start();
    }

}
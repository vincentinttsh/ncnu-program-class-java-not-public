/**
 * Interface
 */
interface Listener{
    int x = 0; //x is public final
    int callme();// is public abstract
}
interface AnotherListener{

}
public abstract class Interface {
    public void setX(){

    }
    public abstract void setY();
    public static void main(String[] args) {
        Interface x = new B();
    }
}

class B extends Interface implements Listener{
    public void setY(){

    }
    public int callme(){
        return 100;
    }
}

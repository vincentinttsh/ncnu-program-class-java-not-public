import java.util.Scanner;
public class Marathon {
                                //每個人能跑的,目標公里數,抓的人數,抓到哪,被抓的人
    private static void to_move(int[] data, int target, int now, int pos, int[] have){
        if(target < 0)
            return;
        if(target == 0){
            for (int i = 0 ; i < now ; i++)
                System.out.print(have[i] + " ");
            System.out.println();
            return;
        }
        for (int i = pos ; i < data.length ; i++){
            have[now] = data[i];
            to_move(data, target - data[i], now + 1, i + 1, have);
        }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt(); //人數
        int target = input.nextInt();//目標
        int[] can_run = new int[n];//每個人能跑的
        for (int i = 0 ; i < can_run.length ; i++) 
            can_run[i] = input.nextInt();
        to_move(can_run, target, 0, 0, new int[n]);
        input.close();
    }
}
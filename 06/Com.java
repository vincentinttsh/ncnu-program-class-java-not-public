import java.util.Scanner;
public class Com {
    static void perm(char[] item , char[] have , int now){
        if (now == have.length){
            for (char x : have)
                System.out.print(x);
            System.out.println();
            return;
        }
        for (int i = 0 ; i < item.length ; i++){
            boolean ishave = false;
            for (int j = 0 ; j < now ; j++)
                if(have[j] == item[i]){
                    ishave = true;
                    break;
                }
            if(ishave == false){
                have[now] = item[i];
                perm(item, have, now + 1);
            }
        }
    }
    static void comb(char[] item , char[] have , int now , int pos){
        if (now == have.length){
            for (char x : have)
                System.out.print(x);
            System.out.println();
            return;
        }
        for (int i = pos ; i < item.length ; i++){
            have[now] = item[i];
            comb(item, have, now + 1, i + 1);
        }
    }
    public static void main(String[] args) {
        char[] item = {'a','b','c','d','e'};
        //comb(item, new char[3], 0, 0);
        perm(item, new char[5], 0);
    }
}
class NagetiveException extends Exception{
    public NagetiveException(String msg){
        super(msg);
    }
}
public class TestException {
    public static double sqrt(double x) throws Exception{
        if (x < 0){
            throw new NagetiveException("num must be >=0");
        }
        return Math.sqrt(x);
    }
    public static void main(String[] args) {
        try {
            System.out.println(sqrt(-11));
        }catch (NumberFormatException e){
        }catch (NagetiveException e) {
            System.out.println(e);
        }catch(Exception e){
            System.out.println("three"+e);
        }finally{
            System.out.println("done");
        }
        
            
    }
}
public class Queue_Advance {
    private int size;
    private Node head, tail;
    public synchronized void append(int x){
        while (size >=1000){// not only one thread will do this
            try {
                wait();
            } catch (Exception e) {}
        }
        Node tmp = new Node();
        tmp.data = x;
        if (tail == null) // the first
            head = tail = tmp;
        else
            tail.next = tmp;
        tail = tmp;
        size++;
        if (size == 1){
            notifyAll(); // wake up all threads
        }
    }
    public synchronized int pop(){
        while(size == 0){ 
            try{
                wait();
            }catch(Exception err){}
        }
        Node tmp = head;
        head = head.next;
        if (head == null)
            tail = null;
        if(size == 1000){
            notifyAll();
        }
        size--;
        return tmp.data;
    }
    public synchronized boolean isempty(){
        return size == 0;
    }
    public static void main(String[] args) {
        Queue data = new Queue();
        for (int i =0 ; i < 10 ;i++)
            data.append(i);
        while(!data.isempty())
            System.out.println(data.pop());
    }
}
class Node{
    int data;
    Node next;
}
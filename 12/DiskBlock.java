/*
read lock
write lock
priority policies :
first come first serve
*/

public class DiskBlock {
    private int reader, writer;
    private long readyToken, tokenMachine=1;
    private synchronized getReadLock(){
        int myToken = tokenMachine++;
        while (writer > 0 || myToken > readyToken){
            try {
                wait();
            } catch (Exception e) {
            }
        }
        reader++;
        readyToken++;
        notifyAll();
    }
    private synchronized getWriteLock(){
        int myToken = tokenMachine++;
        while (writer > 0 || reader > 0 || myToken > readyToken) {
            try {
                wait();
            } catch (Exception e) {
            }
        }
        writer++;
        readyToken++;
    }
    private synchronized releaseReadLock(){
        reader--;
        notifyAll();
    }
    private synchronized releaseWriteLock(){
        writer--;
        notifyAll();
    }
    public void read(){
        getReadLock(); 
        // do any read you want
        releaseReadLock();
    }
    public void write(){
        getWriteLock(); 
        // do any write operations you want
        releaseWriteLock();
    }
}
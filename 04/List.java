import java.util.Scanner;

/**
 * List
 */
public class List {
    private int[] data;
    private int now_size = 1 , how_many = 0;
    private void New_size() {
        int[] temp1 = data;
        data = new int[now_size];
        for (int i = 0 ; i < how_many ; i++ ){
            data[i] = temp1[i];
        }
    }
    private void qsort(int[] data,int left,int right) {
        if (left >= right)
            return;
        int v = data[left] , l , r;
        for(l = left , r = right+1 ; l < r ; ){
            while(++l < right && data[l] <= v);
            while(--r >= l && data[r] >= v);
            if(l <= r){
                int temp = data[r];
                data[r] = data[l];
                data[l] = temp;
            }
        }
        data[left] = data[r];
        data[r] = v;
        qsort(data, left, r);
        qsort(data, r+1, right);
    }
    public List(){
        New_size();
    }
    public void Append(int x){
        if (how_many == now_size){
            now_size *= 2;
            New_size();
        }
        data[how_many++] = x ;
    }
    
    public void Insert(int pos , int x){
        if (pos >= how_many){
            System.out.println("error, pos is bigger than now have");
            return;
        }
        if (how_many == now_size){
            now_size *= 2;
            New_size();
        }
        for(int i = how_many ; i > pos ; i--){
            data[i] = data[i-1];
        }
        data[pos]=x;
        how_many++;
    }

    public int Get(int index){
        if (index < how_many)
            return data[index];
        else{
            System.out.println("error, pos is bigger than now have");
            return -1;
        }
    }
    public void Show(){
        for (int i = 0 ; i < how_many; i++)
            System.out.println("index: " + i + ", data: " + data[i]);
    }
    public void Sort(){
        qsort(data, 0, how_many-1);
    }
    public void Show_size(){
        System.out.println("Now size : " + now_size +", " + how_many + " data");
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        List data = new List();
        while(input.hasNext()){
            String command = input.next();
            if (command.equals("get"))
                System.out.println(data.Get(input.nextInt()));
            else if(command.equals("add"))
                data.Append(input.nextInt());
            else if(command.equals("insert"))
                data.Insert(input.nextInt() , input.nextInt());
            else if(command.equals("size"))
                data.Show_size();
            else if(command.equals("sort"))
                data.Sort();
            else if(command.equals("show"))
                data.Show();
            else if(command.equals("exit"))
                break ;
            else
                System.out.println("無此指令");
        }
        input.close();
    }
}
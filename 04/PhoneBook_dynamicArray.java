import java.util.Scanner;
class PhoneData_dynamicArray{
    private String[] number ,name;
    private int now_size = 1 , how_many = 0;
    private void New_size() {
        String[] temp1 = number , temp2 = name;
        number = new String[now_size];
        name = new String[now_size];
        for (int i = 0 ; i < how_many ; i++ ){
            number[i] = temp1[i];
            name[i] = temp2[i];
        }
    }
    public PhoneData_dynamicArray(){
        New_size();
    }
    public void Add_person(String name , String number) {
        if (how_many == now_size){
            now_size *= 2;
            New_size();
        }
        this.name[how_many] = name ;
        this.number[how_many++] = number ;
    }
    public void Find(String name){
        for (int i = 0 ; i < how_many; i++)
            if (this.name[i].equals(name)){ 
                System.out.println("name: " + this.name[i] + ", phone: " + this.number[i]);
                return; //找到就結束
            }
        System.out.println("查無此人"); // 因有return, 找不到才會跑這行
    }

    public void Show(){
        for (int i = 0 ; i < how_many; i++)
            System.out.println("name: " + this.name[i] + ", phone: " + this.number[i]);
    }

    public void Show_size(){
        System.out.println("Now size : " + now_size +", " + how_many + " people");
    }
}
public class  PhoneBook_dynamicArray{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //創造電話簿
        PhoneData_dynamicArray data = new PhoneData_dynamicArray(); 
        while(input.hasNext()){
            String command = input.next();
            if (command.equals("show"))
                data.Show();
            else if(command.equals("find"))
                data.Find(input.next()); //得到尋找的人名
            else if(command.equals("add"))
                data.Add_person(input.next() , input.next());
            else if(command.equals("size"))
                data.Show_size();
            else if(command.equals("exit"))
                break ;
            else
                System.out.println("無此指令");
        }
        input.close();
    }
}

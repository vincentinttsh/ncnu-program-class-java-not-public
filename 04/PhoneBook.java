import java.util.Scanner;

class PhoneData {
    private String[] number; //電話號碼
    private String[] name; //姓名

    public PhoneData(int n) { // 多少人創造多少陣列
        name = new String[n];
        number = new String[n];
    }

    public void Add_person(int no , String name, String number) { //加入電話簿
        this.name[no] = name;
        this.number[no] = number;
    }

    public void Find(String name){
        for (int i = 0 ; i < this.name.length; i++)
            if (this.name[i].equals(name)){ 
                System.out.println("name: " + this.name[i] + ", phone: " + this.number[i]);
                return; //找到就結束
            }
        System.out.println("查無此人"); // 因有return, 找不到才會跑這行
    }

    public void Show(){
        for (int i = 0 ; i < this.name.length; i++)
            System.out.println("name: " + this.name[i] + ", phone: " + this.number[i]);
    }

}

public class PhoneBook {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt(); // n個人
        //創造電話簿
        PhoneData data = new PhoneData(n); 
        for(int i = 0 ; i < n ; i ++)
            data.Add_person(i, input.next() , input.next());
        while(input.hasNext()){
            String command = input.next();
            if (command.equals("show"))
                data.Show();
            else if(command.equals("find"))
                data.Find(input.next()); //得到尋找的人名
            else
                System.out.println("無此指令");
        }
        input.close();
    }
}

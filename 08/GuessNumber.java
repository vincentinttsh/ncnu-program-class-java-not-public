import java.util.Scanner;
public class GuessNumber {
    int ans[], guessTimes; 
    boolean have_num[]; //ans有的數字
    public GuessNumber(){
        int temp;
        this.guessTimes = 0;
        this.ans = new int[4];
        boolean[] have = new boolean[10];
        for(int i = 0 ; i < this.ans.length ; i++){//填入數字
            do{//產生亂數直到該數位出現過
                temp = (int)(10*Math.random());
            }while(have[temp]);
            this.ans[i] = temp;
            have[temp] = true;
        }
        have_num = have;
    }
    private boolean Illegal(String guess){ //數字重複
        boolean[] have = new boolean[10];
        for(int i = 0 ; i < guess.length(); i++){
            if(have[guess.charAt(i)-'0'])
                return true;
            have[guess.charAt(i)-'0'] = true;
        }
        return false;
    }
    private int Result(String guess){//回傳幾A幾B
        int a = 0, b = 0;
        for(int i = 0 ; i < this.ans.length ; i++){
            if(guess.charAt(i) - '0' == this.ans[i])
                a++;
            if(this.have_num[guess.charAt(i) - '0'])
                b++;
        }
        return a * 10 + b - a;
    }
    public void Play(){
        Scanner input = new Scanner(System.in);
        int code;
        String guess;
        do{
            this.guessTimes++;
            System.out.print("請輸入一串數字： ");
            guess = input.next();
            while(Illegal(guess) || guess.length() != 4){
                System.out.println("Illegal input!");
                System.out.print("請輸入一串數字： ");
                guess = input.next();
            }
            code = Result(guess);
            if(code != 40)
                System.out.println(code / 10 +"A, "+code % 10 + "B");
        }while(code != 40);
        System.out.println("恭喜猜中，你猜了" + guessTimes+"次");
        input.close();
    }
    public static void main(String[] args) {
        GuessNumber game = new GuessNumber();
        game.Play();
    }
}
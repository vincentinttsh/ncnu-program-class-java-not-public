import java.util.Scanner;
public class Bingo {
    private boolean[] visited; //該數是否以被猜過
                //斜線1, 斜線2, 有幾條已連線, 幾格, 該數字位置, 該col有幾個數字以被猜到, 該row有幾個數字以被猜到, 數字
    private int ds, us, line, size, where[], col[], row[], map[][];
    public Bingo(int n){
        this.size = n;
        this.ds = this.us = this.line = 0;
        this.map = new int[n][n];
        this.visited = new boolean[n*n+1];
        this.where = new int[n*n+1];
        this.col = new int[n];
        this.row = new int[n];
        for (int i=0;i<n;i++) 
            col[i] = row[i] = 0;
        boolean[] have = new boolean[n*n+1];
        have[0] = true;
        int temp;
        for(int i = 0 ; i < n ;i++)//填入數字
            for(int j = 0 ; j < n ; j++){
                do{//產生亂數直到該數位出現過
                    temp = (int)(Math.random() * n * n)+1;
                }while(have[temp]);
                this.map[i][j] = temp;
                this.where[temp]=i*n+j;
                have[temp] = true;
            }
    }
    public boolean game_over(){
        if(this.line >= 5)
            return true;
        return false;
    }
    private boolean illegal_not_in_size(int num){
        if(num <= 0 || num >=this.visited.length)
            return true;
        return false;
    }
    private boolean illegal_same(int num){
        if(this.visited[num])
            return true;
        return false;
    }
    public int guess(int num){
        if(illegal_not_in_size(num))
            return 1;
        if(illegal_same(num))
            return 2;
        this.visited[num] = true;
        //this.where[num] / this.size 位置1, this.where[num] % this.size 位置2
        if(this.where[num] / this.size == this.where[num] % this.size && ++this.us == this.size)
            this.line+=1;
        if(this.where[num] / this.size + this.where[num] % this.size == this.size-1 && ++this.ds == this.size)
            this.line+=1;
        if(++this.row[this.where[num] / this.size] == this.size)
            this.line+=1;
        if(++this.col[this.where[num] % this.size] == this.size)
            this.line+=1;
        return 3;
    }
    public void print(){
        for(int[] x : this.map){
            for(int y : x){
                if(this.visited[y])
                    System.out.printf("%02d ", y);
                else
                    System.out.print("** ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int code = 4, times = 0;
        Bingo game = new Bingo(input.nextInt());
        while(!game.game_over()){
            times+=1;
            do{
                System.out.print("You guess: ");
                code = game.guess(input.nextInt());
                if (code == 1)
                    System.out.println("輸入不符合規定的數字");
                else if(code == 2)
                    System.out.println("輸入重複的數");
                
            }while(code != 3);
            game.print();
        }
        System.out.println("You guess "+times+" times");
        input.close();
    }
}